import React from 'react';
import { Theme } from '@mui/material/styles';
import { createStyles } from '@mui/styles';
import { WithStyles, withStyles } from '@mui/styles';
import withRoot from './withRoot';
import { grey, amber, blue, indigo, brown, lightGreen, blueGrey, green, deepPurple, cyan, teal, lime } from '@mui/material/colors';
import { createTheme, ThemeProvider, styled } from '@mui/material/styles';
import {
  DataGridPro, GridFilterModel, GridApiRef, GridColumnVisibilityModel, GridPinnedColumns, GridSortItem, GridColumnResizeParams, GridEditRowsModel
  , GridSelectionModel, GridDensity, GridColumnsPanel,  GridRowGroupingModel
} from '@mui/x-data-grid-pro';
import { withData } from './withData';
import DataGridContainerProToolbar from './DataGridContainerProToolbar';
import DataGridContainerProNoRows from './DataGridContainerProNoRows';
import DataGridContainerProFooter from './DataGridContainerProFooter';
import { SavedView, ColumnOrder, ColumnSize } from './DataGridModels'
import Grid from '@mui/material/Grid';
import Stack from '@mui/material/Stack';
import Paper from '@mui/material/Paper';
import { GridRowParams } from '@mui/x-data-grid';

var $ = require("jquery");
var _ = require('lodash');
const theme = createTheme({
  components: {
    MuiPaper: {
      styleOverrides: {
        root: {
          backgroundColor: amber[500]
        }
      }
    }
  },
});
const appBarReg = 64;
const appBarSmall = 56
const tabReg = 48;
let styles = (theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      height: "100%"
    },
    appBar: {
    },
    logoContainer: {
      [theme.breakpoints.down("xs")]: {
        display: 'none'
      },
      paddingRight: 24
    },
    logoImage: {
      height: 32,
    },
    content: {
      flexGrow: 1,
      overflow: "auto",
      backgroundColor: theme.palette.background.default,

    },
    noButtonWrap: {
      whiteSpace: "nowrap",
      minWidth: "max-content"
    },
    mainContent: {
      padding: 24,
      height: `calc(100vh - ${appBarReg + tabReg}px)`,
      overflow: "auto",
      [theme.breakpoints.down("xs")]: {
        height: `calc(100vh - ${appBarSmall + tabReg}px)`,
      },
    },
    columnsPanelRow :{ // 1: Expand Button 2: Select Button 3: Row ID
      "&:nth-child(1)": {
        display:"none !important",
      },
      "&:nth-child(2)": {
        display:"none !important",
      },
      "&:nth-child(3)": {
        display:"none !important",
      },
    }
  });

const baseVisibilityModel = { id: false };
class DataGridContainerPro extends React.Component<WithStyles<typeof styles> & {
  data: any;
  simpleData: any;
  originalData: any;
  apiRef: GridApiRef;
}, {
  filterModel: GridFilterModel;
  sortModel: GridSortItem[];
  columnVisibilityModel: GridColumnVisibilityModel;
  savedViews: SavedView[],
  data: any;
  rows: any[];
  pinnedColumns: GridPinnedColumns;
  selectedView: SavedView;
  currentResized: ColumnSize[];
  editRowsModel: GridEditRowsModel;
  selectionModel: GridSelectionModel;
  detailWidth: number;
  searchText: string;
  density: GridDensity;
  densityOverride: GridDensity;
  groupingModel: GridRowGroupingModel;
}> {
  constructor(props: any) {
    super(props);
    let columnOrder: ColumnOrder[] = this.props.data.columns.map((col: any, index: any) => {
      return {
        field: col.field,
        order: index
      }
    })
    const view: SavedView = {
      name: "Default",
      pinnedColumns: {},
      columnOrder: columnOrder,
      filterModel: { items: [] },
      sortModel: [],
      columnSize: [],
      columnVisibilityModel: baseVisibilityModel,
      density: "standard",
      groupingModel: []
    };
    const defaultViews = [view];
    this.state = {
      sortModel: [],
      currentResized: [],
      filterModel: { items: [] },
      columnVisibilityModel: baseVisibilityModel,
      savedViews: [...defaultViews],
      data: this.props.data,
      rows: this.props.data.rows,
      pinnedColumns: {},
      selectedView: defaultViews[0],
      editRowsModel: {},
      selectionModel: [],
      groupingModel:[],
      detailWidth: 0,
      searchText: "",
      density: "standard",
      densityOverride: "standard"
    };
  }
  componentDidUpdate = (prevProps: any) => {
    const { classes, apiRef } = this.props;
    if (prevProps.data.rows.length != this.props.data.rows.length) {
      let dimensions = apiRef.current.getRootDimensions();
      if (dimensions) this.setState({ detailWidth: dimensions.viewportInnerSize.width })
      console.log(this.props.data)
      this.setState({ data: this.props.data, rows: this.props.data.rows })
    }
  }

  private _updator = (state: any, fc?: Function): void => {
    if (fc) this.setState(state, () => { fc() });
    else this.setState(state);
  }
  saveView = (viewName: string) => {
    const { classes, apiRef } = this.props;
    let { groupingModel, density, filterModel, savedViews, data, columnVisibilityModel, pinnedColumns, selectedView, sortModel, currentResized } = this.state;
    let columnOrder: ColumnOrder[] = apiRef.current.getAllColumns().map((col, index) => {
      return {
        field: col.field,
        order: index
      }
    })
    let view = {
      sortModel: sortModel,
      filterModel: filterModel,
      name: viewName,
      columnVisibilityModel: columnVisibilityModel,
      columnOrder: columnOrder,
      columnSize: currentResized,
      pinnedColumns: pinnedColumns,
      density: density,
      groupingModel:groupingModel
    }
    let savedViewsFiltered = savedViews.filter((view) => { return view.name !== viewName })
    this.setState({
      savedViews: [...savedViewsFiltered, view],
      selectedView: view,
    })
  }
  onViewSet = (view: SavedView) => {
    const { classes, apiRef, originalData } = this.props;
    let { filterModel, savedViews, data, columnVisibilityModel, pinnedColumns, selectedView } = this.state;
    let orderedColumns = Array.from(originalData.columns)
    orderedColumns = orderedColumns.sort((a: any, b: any) => {
      let foundA = view.columnOrder.filter(c => c.field === a.field)[0];
      let foundB = view.columnOrder.filter(c => c.field === b.field)[0];
      if (foundA.order > foundB.order) return 1;
      else if (foundA.order < foundB.order) return -1;
      else return 0;
    })
    /* orderedColumns = orderedColumns.map((column: any) => {
      let foundResize = view.columnSize.filter(s => s.field === column.field)[0];
      if (foundResize) {
        column.width = foundResize.size;
      }
      return column
    })
    */
    let columnsUpdatedData = _.clone(originalData);
    columnsUpdatedData.columns = orderedColumns;
    this.setState({ groupingModel:view.groupingModel,densityOverride: view.density, density: view.density, selectedView: view, currentResized: [], sortModel: view.sortModel, data: columnsUpdatedData, filterModel: view.filterModel, pinnedColumns: view.pinnedColumns, columnVisibilityModel: view.columnVisibilityModel })
  }
  requestSearch = (searchValue: string) => {
    let { data } = this.state;
    const searchRegex = new RegExp(escapeRegExp(searchValue), 'i');
    const filteredRows = data.rows.filter((row: any) => {
      return Object.keys(row).some((field) => {
        let result = false;
        result = searchRegex.test(row[field] && row[field].toString());
        return result
      });
    });
    this.setState({ rows: filteredRows, searchText: searchValue });
  };

  render() {
    const { classes, apiRef, simpleData } = this.props;
    let { groupingModel, densityOverride, density, detailWidth, selectionModel, editRowsModel, searchText, rows, data, filterModel, columnVisibilityModel, pinnedColumns, sortModel, currentResized, selectedView, savedViews } = this.state;
    return (
      <div style={{ height: '100%', width: '100%' }}>
        <DataGridPro
          apiRef={apiRef}
          rows={rows}
          sx={{
            ".MuiDataGrid-virtualScroller": {
              backgroundColor: grey[800]
            },
          }}
          classes={{
            columnsPanelRow :classes.columnsPanelRow 
          }}
          columns={data.columns}
          columnBuffer={2} columnThreshold={2}
          filterModel={filterModel}
          loading={data.rows.length === 0}
          checkboxSelection
          density={density}
          disableSelectionOnClick
          components={{
            Toolbar: DataGridContainerProToolbar,
            NoRowsOverlay: DataGridContainerProNoRows,
            NoResultsOverlay: DataGridContainerProNoRows,
            Footer: DataGridContainerProFooter
          }}
          componentsProps={{
            toolbar: {
              searchText: searchText,
              requestSearch: this.requestSearch,
              saveView: this.saveView,
              onViewSet: this.onViewSet,
              selectedView: selectedView,
              savedViews: savedViews
            },
            noRowsOverlay: {
              searchText: searchText,
            },
            noResultsOverlay: {
              searchText: searchText,
            },
            footer: {
              api:apiRef,
              selectionModel:selectionModel
            }
          }}
          rowBuffer={10}
          onFilterModelChange={(model, details) => [
            this.setState({ filterModel: model })
          ]}
          columnVisibilityModel={columnVisibilityModel}
          onColumnVisibilityModelChange={(newModel) =>{
            newModel.id = false;
            this.setState({ columnVisibilityModel: newModel })
          }}
          experimentalFeatures={{
            rowGrouping: true,
          }}     
          isRowSelectable={(params:any)=>{
            if (params.id.indexOf("auto-generated-row") >=0) return false
            else return true;
          }}
          pinnedColumns={pinnedColumns}
          onPinnedColumnsChange={(pinnedColumns) => {
            this.setState({ pinnedColumns: pinnedColumns })
          }}
          sortModel={sortModel}
          onSortModelChange={(newSortModel) => {
            this.setState({ sortModel: newSortModel })
          }}
          onColumnWidthChange={(params: GridColumnResizeParams, event: any, details: any) => {
            let currentResizedUpdated = currentResized.filter((c) => {
              return c.field !== params.colDef.field;
            })
            let newResize: ColumnSize = { field: params.colDef.field, size: params.width }
            currentResizedUpdated = [...currentResizedUpdated, newResize];
            this.setState({ currentResized: currentResizedUpdated })
          }}
          getRowClassName={(params: any) => { return classes.appBar }}
          editRowsModel={editRowsModel}
          onEditRowsModelChange={(model) => {
            console.log(model);
            console.log(Object.keys(model)[0])
            this.setState({ editRowsModel: model })
          }}
          onSelectionModelChange={(newSelectionModel) => {
            console.log(newSelectionModel)
            this.setState({ selectionModel: newSelectionModel });
          }}
          selectionModel={selectionModel}
          onDetailPanelExpandedRowIdsChange={() => {

          }}
          onRowGroupingModelChange={(model:GridRowGroupingModel)=>{
            this.setState({groupingModel:model})
          }}
          rowGroupingModel={groupingModel}
          getDetailPanelHeight={() => { return 240 }}
          getDetailPanelContent={(params:any) => {
            if (params.id.indexOf("auto-generated-row") >=0) return null
            return (
              <Stack
                sx={{
                  py: 2,
                  height: 240,
                  boxSizing: 'border-box',
                  position: 'sticky',
                  left: 0,
                  width: detailWidth,
                }}
                direction="column"
              >
                <Paper sx={{ flex: 1, mx: 'auto', width: '90%', p: 1 }}>
                  <Stack direction="column" spacing={1} sx={{ height: 1 }}>
                    <DataGridPro
                      density={"compact"}
                      hideFooter={true}
                      style={{ backgroundColor: grey[900] }}
                      rows={simpleData.rows}
                      columns={simpleData.columns}
                      columnBuffer={2} columnThreshold={2}
                      loading={simpleData.rows.length === 0}
                      sx={{
                        flex: 1,
                        ".MuiDataGrid-virtualScroller": {
                          backgroundColor: `${grey[900]} !important`
                        }
                      }}
                    />
                  </Stack>
                </Paper>
              </Stack>
            )
          }}
        />
      </div>
    );
  }
}
export default withData(withRoot(withStyles(styles)(DataGridContainerPro)));
function escapeRegExp(value: any) {
  return value.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
}