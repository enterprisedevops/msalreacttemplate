import React from 'react';
import { Theme } from '@mui/material/styles';
import { createStyles } from '@mui/styles';
import { WithStyles, withStyles } from '@mui/styles';
import withRoot from './withRoot';
import { grey, amber, blue, indigo, brown, lightGreen, blueGrey, green, deepPurple, cyan, teal, lime } from '@mui/material/colors';
import axios from 'axios';
import { Buffer } from 'buffer';

import Typography from '@mui/material/Typography';
import Avatar from '@mui/material/Avatar';
import Hidden from '@mui/material/Hidden';
import Button from '@mui/material/Button';
import Toolbar from '@mui/material/Toolbar';
import AppBar from '@mui/material/AppBar';
import IconButton from '@mui/material/IconButton';
import DialogContentText from '@mui/material/DialogContentText';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import FormGroup from '@mui/material/FormGroup';
import Checkbox from '@mui/material/Checkbox';
import Slider from '@mui/material/Slider';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormLabel from '@mui/material/FormLabel';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import TextField from '@mui/material/TextField';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';


import { DataGrid, GridToolbar  } from '@mui/x-data-grid';
import {withData} from './withData';

import logo from './Images/logo.svg';

import { MsalProvider } from "@azure/msal-react";
import { IPublicClientApplication, PublicClientApplication, EventType, EventMessage, AuthenticationResult } from "@azure/msal-browser";


const appBarReg = 64;
const appBarSmall = 56
const tabReg = 48;
let styles = (theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      height: "100%"
    },
    appBar: {
      backgroundColor: grey[900]
    },
    logoContainer: {
      [theme.breakpoints.down("xs")]: {
        display: 'none'
      },
      paddingRight: 24
    },
    logoImage: {
      height: 32,
    },
    content: {
      flexGrow: 1,
      overflow: "auto",
      backgroundColor: theme.palette.background.default,

    },
    noButtonWrap: {
      whiteSpace: "nowrap",
      minWidth: "max-content"
    },
    mainContent: {
      padding: 24,
      height: `calc(100vh - ${appBarReg + tabReg}px)`,
      overflow: "auto",
      [theme.breakpoints.down("xs")]: {
        height: `calc(100vh - ${appBarSmall + tabReg}px)`,
      },
    }
  });
class DataGridContainer extends React.Component<WithStyles<typeof styles> & {
  data:any;
}, {
}> {
  constructor(props: any) {
    super(props);
    this.state = {
      userFullName: "",
      userImage: "",
      tab: 0,
      dialogOpen: false,
      snackOpen: false,
      sqlData: []
    };
  }
  async componentDidMount() {
  }

  render() {
    const { classes, data } = this.props;
    const baseFilter:any = {
      items: [{
        id: 1,
        columnField: "commodity",
        value: "tesst",
        operatorValue: "contains"
      },{
        id: 2,
        columnField: "commodity",
        value: "tessta",
        operatorValue: "contains"
      }],
      linkOperator: "or"
    };
    return (
      <div style={{ height: '100%', width: '100%' }}>
        <DataGrid
        style={{backgroundColor:grey[700]}}
          rows={data.rows}
          columns={data.columns}
          autoPageSize 
          loading={data.rows.length === 0}
          checkboxSelection
          columnBuffer={2} columnThreshold={2}
          disableSelectionOnClick
          components={{ Toolbar: GridToolbar }} 
        />
      </div>
    );
  }
}
export default withData(withRoot(withStyles(styles)(DataGridContainer)));
