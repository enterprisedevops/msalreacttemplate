import React from 'react';
import { Theme } from '@mui/material/styles';
import { createStyles } from '@mui/styles';
import { WithStyles, withStyles } from '@mui/styles';
import withRoot from './withRoot';
import { grey, amber, blue, indigo, brown, lightGreen, blueGrey, green, deepPurple, cyan, teal, lime } from '@mui/material/colors';
import { createTheme, ThemeProvider, styled } from '@mui/material/styles';

import { GridOverlay, GridSelectionModel } from '@mui/x-data-grid-pro';
import Box from '@mui/material/Box';

let styles = (theme: Theme) =>
  createStyles({
  });
class DataGridContainerProFooter extends React.Component<WithStyles<typeof styles> & {
  api: any;
  selectionModel: GridSelectionModel
}, {
}> {
  constructor(props: any) {
    super(props);
    this.state = {
    };
  }
  getRowLabel = (size:number) => {
    if (size === 1) return `Total Rows:${size} row`;
    else return `${size} rows`
  }
  render() {
    const { api, selectionModel } = this.props;
    return (
      <div style={{ padding: 8, display:"flex" }}>
        {selectionModel.length > 0 && <span>{(selectionModel.length).toLocaleString()} {selectionModel.length === 1 ? "row" : "rows"} selected</span>}
        <div style={{ flexGrow: 1 }} />
        Total Rows: {(api.current.getVisibleRowModels().size).toLocaleString()}
      </div>
    );
  }
}
export default withRoot(withStyles(styles)(DataGridContainerProFooter));
