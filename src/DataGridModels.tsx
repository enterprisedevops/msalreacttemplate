
import { GridFilterModel, GridColumnVisibilityModel,GridPinnedColumns,GridSortItem,GridDensity,GridRowGroupingModel  } from '@mui/x-data-grid-pro';

export type SavedView = {
  filterModel: GridFilterModel;
  columnVisibilityModel: GridColumnVisibilityModel;
  columnOrder: ColumnOrder[];
  columnSize: ColumnSize[];
  name: string;
  pinnedColumns: GridPinnedColumns;
  sortModel: GridSortItem[];
  density:GridDensity ;
  groupingModel:GridRowGroupingModel;
}
export type ColumnOrder = {
  order: number;
  field: string;
}
export type ColumnSize = {
  size: number;
  field: string;
}