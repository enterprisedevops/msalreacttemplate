// App.tsx
import React, { useEffect } from 'react';
import {
  AuthenticatedTemplate,
  UnauthenticatedTemplate,
  useMsal,
  useIsAuthenticated,
} from '@azure/msal-react';
import { loginRequest } from './authConfig';
import App from './App';
import CircularProgress from '@mui/material/CircularProgress';
import Backdrop from '@mui/material/Backdrop';
import amber from '@mui/material/colors/amber';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import {
  IPublicClientApplication, PublicClientApplication, EventType, EventMessage, AuthenticationResult,
  InteractionStatus, InteractionType, InteractionRequiredAuthError, AccountInfo
} from "@azure/msal-browser";

const AppLogin = () => {
  const isAuthenticated = useIsAuthenticated();
  const { instance, inProgress } = useMsal();
  useEffect(() => {
    const accounts = instance.getAllAccounts();
    if (accounts.length > 0) {
      instance.setActiveAccount(accounts[0]);
    }

    instance.addEventCallback((event: any) => {
      // set active account after redirect
      if (event.eventType === EventType.LOGIN_SUCCESS && event.payload.account) {
        const account = event.payload.account;
        instance.setActiveAccount(account);
      }
    });

    console.log('get active account', instance.getActiveAccount());

    // handle auth redired/do all initial setup for msal
    instance.handleRedirectPromise().then(authResult => {
      // Check if user signed in 
      const account = instance.getActiveAccount();
      if (!account && inProgress === InteractionStatus.None) {
        // redirect anonymous user to login page 
        instance.loginRedirect();
      }
    }).catch(err => {
      // TODO: Handle errors
      console.log(err);
    });
  });

  return (
    <Router>
      <Switch>
        <Route>
          <AuthenticatedTemplate>
            <App instance={instance} />
          </AuthenticatedTemplate>
          <UnauthenticatedTemplate>
            <Backdrop style={{ color: amber[500], zIndex: 3000, backgroundColor: "rgba(0, 0, 0, 0.8)" }} open={true} transitionDuration={0}>
              <CircularProgress color="inherit" />
            </Backdrop>
          </UnauthenticatedTemplate>
        </Route>
      </Switch>
    </Router>
  );
};
export default AppLogin;