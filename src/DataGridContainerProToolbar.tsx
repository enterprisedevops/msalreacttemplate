import React from 'react';
import { Theme } from '@mui/material/styles';
import { createStyles } from '@mui/styles';
import { WithStyles, withStyles } from '@mui/styles';
import withRoot from './withRoot';
import { grey, amber, blue, indigo, brown, lightGreen, blueGrey, green, deepPurple, cyan, teal, lime } from '@mui/material/colors';
import Button from '@mui/material/Button';
import MenuItem from '@mui/material/MenuItem';
import TextField from '@mui/material/TextField';
import BookmarkAdd from '@mui/icons-material/BookmarkAdd';
import Bookmark from '@mui/icons-material/Bookmark';
import SearchIcon from '@mui/icons-material/Search';
import ClearIcon from '@mui/icons-material/Clear';
import Popover from '@mui/material/Popover';
import IconButton from '@mui/material/IconButton';
import Grow from '@mui/material/Grow';
import InputAdornment from '@mui/material/InputAdornment';
import Grid from '@mui/material/Grid';
import { createTheme, ThemeProvider, styled } from '@mui/material/styles';

import { GridToolbarColumnsButton, GridToolbarDensitySelector, GridToolbarFilterButton, GridToolbarContainer } from '@mui/x-data-grid-pro';
import { withData } from './withData';
import { SavedView, ColumnOrder, ColumnSize } from './DataGridModels'

let styles = (theme: Theme) =>
  createStyles({
  });

const baseVisibilityModel = { id: false };
class DataGridContainerProToolbar extends React.Component<WithStyles<typeof styles> & {
  selectedView: SavedView;
  savedViews: SavedView[],
  onViewSet: Function;
  saveView: Function;
  requestSearch: Function;
}, {
  newViewAnchorEl?: any;
  viewName: string;
  newViewOpen: boolean;
  searchText: string
}> {
  constructor(props: any) {
    super(props);
    this.state = {
      newViewOpen: false,
      viewName: "",
      searchText: ""
    };
  }
  componentDidMount = () => {
    console.log("mounted")
  }
  interval: any = null;
  render() {
    const { selectedView, savedViews, onViewSet, saveView, requestSearch } = this.props;
    let { viewName, newViewOpen, newViewAnchorEl, searchText } = this.state;
    return (
      <GridToolbarContainer>
        <Grid container spacing={1} style={{ paddingLeft: 4 }}>
          <Grid item><GridToolbarFilterButton componentsProps={{ button: { variant: "contained", color: "secondary" } }} /></Grid>
          <Grid item><GridToolbarColumnsButton color={"secondary"} variant="contained" touchRippleRef={null} /></Grid>
          <Grid item><GridToolbarDensitySelector color={"secondary"} variant="contained" touchRippleRef={null} /></Grid>
          <Grid item><Button variant="contained" color={"secondary"} size={"small"} startIcon={<BookmarkAdd />} onClick={(event) => {
            this.setState({ newViewAnchorEl: event.currentTarget, newViewOpen: true })
          }}>Save View</Button></Grid>

        </Grid>
        <div style={{ flexGrow: 1 }}></div>

        <Grid container spacing={1} style={{ marginRight: 4 }} justifyContent={"right"}>
          <Grid item>
            <TextField
              placeholder="Search…" variant="outlined" size={"small"} value={searchText}
              InputProps={{
                startAdornment: <SearchIcon fontSize="small" />,
                endAdornment: (
                  <IconButton
                    title="Clear"
                    aria-label="Clear"
                    size="small"
                    style={{ visibility: searchText !== "" ? 'visible' : 'hidden' }}
                    onClick={() => {
                      this.setState({ searchText: "" })
                      clearTimeout(this.interval)
                      this.interval = setTimeout(() => {
                        requestSearch("")
                      }, 500)
                    }}
                  >
                    <ClearIcon fontSize="small" />
                  </IconButton>
                ),
              }}
              sx={{
                width: {
                  xs: 1,
                  sm: 'auto',
                },
                '& .MuiSvgIcon-root': {
                  mr: 0.5,
                },
                '& .MuiInput-underline:before': {
                  borderBottom: 1,
                  borderColor: 'divider',
                },
              }}
              onChange={(event) => {
                this.setState({ searchText: event.target.value })
                clearTimeout(this.interval)
                this.interval = setTimeout(() => {
                  requestSearch(event.target.value)
                }, 500)
              }} /></Grid>
          <Grid item><TextField fullWidth select
            label=""
            InputProps={{
              startAdornment:
                <InputAdornment position="start">
                  <Bookmark />
                </InputAdornment>
            }}
            color={"primary"}
            hiddenLabel
            value={selectedView.name}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              let foundView = savedViews.filter((view) => view.name === event.target.value)[0];
              onViewSet(foundView)
            }}
            margin={"none"}
            size={"small"} style={{ width: 250 }}>
            {savedViews.map((view: any) => (
              <MenuItem key={view.name} value={view.name}>
                {view.name}
              </MenuItem>
            ))}
          </TextField>
          </Grid>
        </Grid>
        <Popover
          open={newViewOpen}
          anchorEl={newViewAnchorEl}
          onClose={() => { this.setState({ newViewAnchorEl: null, newViewOpen: false }) }}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
        >
          <div style={{ padding: 12, backgroundColor: grey[800] }}>
            <TextField label="View Name" value={viewName}
              autoFocus
              onChange={(event: React.ChangeEvent<HTMLInputElement>) => { this.setState({ viewName: event.target.value }) }} variant="outlined" fullWidth
              InputProps={{
                endAdornment:
                  <Grow in={true} ><IconButton
                    disabled={viewName === ""}
                    onClick={() => { saveView(viewName); this.setState({ newViewAnchorEl: null, newViewOpen: false, viewName: "" }); }}
                    edge="end"
                  >
                    <BookmarkAdd />
                  </IconButton></Grow>
              }} />
          </div>
        </Popover>
      </GridToolbarContainer>
    );
  }
}
export default withRoot(withStyles(styles)(DataGridContainerProToolbar));
