import React from 'react';
import { useDemoData } from '@mui/x-data-grid-generator';
import { useGridApiRef ,GridApiRef } from '@mui/x-data-grid-pro';

export const withData = (Component: any) => {
  return (props: any) => {
    let { data } = useDemoData({
      dataSet: 'Commodity',
      rowLength: 10000,
      editable: true,
    });
    let apiRef = useGridApiRef();
    let columns = [
      { field: 'id', headerName: 'ID', width: 90 },
      {
        field: 'firstName',
        headerName: 'First name',
        width: 150,
        editable: true,
      },
      {
        field: 'lastName',
        headerName: 'Last name',
        width: 150,
        editable: true,
      },
      {
        field: 'age',
        headerName: 'Age',
        type: 'number',
        width: 110,
        editable: true,
      },
      {
        field: 'fullName',
        headerName: 'Full name',
        description: 'This column has a value getter and is not sortable.',
        sortable: false,
        width: 160,
        valueGetter: (params:any) =>
          `${params.row.firstName || ''} ${params.row.lastName || ''}`,
      },
    ];
    
    let rows = [
      { id: 1, lastName: 'Snow', firstName: 'Jon', age: 35 },
      { id: 2, lastName: 'Lannister', firstName: 'Cersei', age: 42 },
      { id: 3, lastName: 'Lannister', firstName: 'Jaime', age: 45 },
      { id: 4, lastName: 'Stark', firstName: 'Arya', age: 16 },
      { id: 5, lastName: 'Targaryen', firstName: 'Daenerys', age: null },
      { id: 6, lastName: 'Melisandre', firstName: null, age: 150 },
      { id: 7, lastName: 'Clifford', firstName: 'Ferrara', age: 44 },
      { id: 8, lastName: 'Frances', firstName: 'Rossini', age: 36 },
      { id: 9, lastName: 'Roxie', firstName: 'Harvey', age: 65 },
    ];
    let simpleData:any = {
      rows:rows,
      columns:columns
    }
    return <Component simpleData={simpleData} data={data} originalData={data} apiRef={apiRef} {...props} />;
  };
};