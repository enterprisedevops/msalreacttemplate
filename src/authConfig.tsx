import { Configuration, PopupRequest } from "@azure/msal-browser";

const clientId = '85a05ab5-2804-4f54-819f-b19ef151b973'; 
const tenantId = 'fe99bbb3-bdc3-4260-998e-c28159c895a2';
const msalScopes = [`api://85a05ab5-2804-4f54-819f-b19ef151b973/access`] // Revert back to clientId after testing
// Config object to be passed to Msal on creation
export const msalConfig: Configuration = {
    auth: {
        clientId: clientId,
        authority: `https://login.microsoftonline.com/${tenantId}/`,
        postLogoutRedirectUri: window.location.origin,
        redirectUri: window.location.origin,
    }
};

// Add here scopes for id token to be used at MS Identity Platform endpoints.
export const loginRequest: PopupRequest = {
    scopes: msalScopes
};

// Add here the endpoints for MS Graph API services you would like to use.
export const graphConfig = {
    graphMeEndpoint: "https://graph.microsoft-ppe.com/v1.0/me"
};