// index.tsx 
// Updated for Jenkins Docker Image version test
// Second Updated for Jenkins Docker Image version test
//Cloud run build test edit
//Sample edit for Jenkins build test
//New pipeline test
import React from 'react';
import ReactDOM from 'react-dom';
import AppLogin from './AppLogin';
import { PublicClientApplication } from '@azure/msal-browser';
import { msalConfig } from './authConfig';
import { MsalProvider } from '@azure/msal-react';
import './index.css';

const msalInstance = new PublicClientApplication(msalConfig);

ReactDOM.render(
  <React.StrictMode>
    <MsalProvider instance={msalInstance}>
      <AppLogin />
    </MsalProvider>
  </React.StrictMode>,
  document.getElementById('root')
);