import * as React from 'react';
import { ThemeProvider , createTheme } from '@mui/material/styles';
import blueGrey from '@mui/material/colors/blueGrey';
import amber from '@mui/material/colors/amber';
import grey from '@mui/material/colors/grey';
import teal from '@mui/material/colors/teal';
import CssBaseline from '@mui/material/CssBaseline';
import type {} from '@mui/x-data-grid/themeAugmentation';
import type {} from '@mui/x-data-grid-pro/themeAugmentation';

const theme = createTheme({
  components: {
    MuiDataGrid: {
      styleOverrides: {
        root: {
          backgroundColor: grey[600],
        },        
      },
    },
  },
  palette: {
    mode:"dark",
    primary: amber,
    secondary: grey,
    background: {
        default:"#303030",
        paper:grey[800]
    }
  },
});
function withRoot<P>(Component: React.ComponentType<P>) {
  function WithRoot(props: P) {
    return (
      <ThemeProvider  theme={theme}>
        <CssBaseline />
        <Component {...props} />
      </ThemeProvider >
    );
  }
  return WithRoot;
}
export default withRoot;
