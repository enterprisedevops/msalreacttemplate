import React from 'react';
import { Theme } from '@mui/material/styles';
import { createStyles } from '@mui/styles';
import { WithStyles, withStyles } from '@mui/styles';
import withRoot from './withRoot';
import { grey, amber, blue, indigo, brown, lightGreen, blueGrey, green, deepPurple, cyan, teal, lime } from '@mui/material/colors';
import axios from 'axios';
import { Buffer } from 'buffer';

import Typography from '@mui/material/Typography';
import Avatar from '@mui/material/Avatar';
import Hidden from '@mui/material/Hidden';
import Button from '@mui/material/Button';
import Toolbar from '@mui/material/Toolbar';
import AppBar from '@mui/material/AppBar';
import IconButton from '@mui/material/IconButton';
import DialogContentText from '@mui/material/DialogContentText';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import FormGroup from '@mui/material/FormGroup';
import Checkbox from '@mui/material/Checkbox';
import Slider from '@mui/material/Slider';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormLabel from '@mui/material/FormLabel';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import TextField from '@mui/material/TextField';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';




import logo from './Images/logo.svg';

import { MsalProvider } from "@azure/msal-react";
import { IPublicClientApplication, PublicClientApplication, EventType, EventMessage, AuthenticationResult } from "@azure/msal-browser";
import DataGridContainer from './DataGridContainer';
import DataGridContainerPro from './DataGridContainerPro';


const appBarReg = 64;
const appBarSmall = 56
const tabReg = 48;
let styles = (theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      height: "100%"
    },
    appBar: {
      backgroundColor: grey[900]
    },
    logoContainer: {
      [theme.breakpoints.down("xs")]: {
        display: 'none'
      },
      paddingRight: 24
    },
    logoImage: {
      height: 32,
    },
    content: {
      flexGrow: 1,
      overflow: "auto",
      backgroundColor: theme.palette.background.default,

    },
    noButtonWrap: {
      whiteSpace: "nowrap",
      minWidth: "max-content"
    },
    mainContent: {
      padding: 24,
      height: `calc(100vh - ${appBarReg + tabReg}px)`,
      overflow: "auto",
      [theme.breakpoints.down("xs")]: {
        height: `calc(100vh - ${appBarSmall + tabReg}px)`,
      },
    }
  });
class App extends React.Component<WithStyles<typeof styles> & {
  instance: IPublicClientApplication;
}, {
  userFullName: string;
  userImage: string;
  tab: number;
  dialogOpen: boolean;
  snackOpen: boolean;
  sqlData:any[];
}> {
  constructor(props: any) {
    super(props);
    this.state = {
      userFullName: "",
      userImage: "",
      tab: 4,
      dialogOpen: false,
      snackOpen: false,
      sqlData:[]
    };
  }
  public axiosConfigImage = (token: string) => {
    return {
      responseType: "arraybuffer" as "text" | "arraybuffer" | "blob" | "document" | "json" | "stream" | undefined,
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        "Authorization": `Bearer ${token}`,
      }
    }
  }
  public axiosConfig = (token: string) => {
    return {
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        "Authorization": `Bearer ${token}`,
      }
    }
  }
  async componentDidMount() {
    const { instance } = this.props;
    let accounts = instance.getAllAccounts();
    if (accounts.length > 0) {
      instance.setActiveAccount(accounts[0]);
      const appToken = await instance.acquireTokenSilent({
        scopes: [`api://85a05ab5-2804-4f54-819f-b19ef151b973/access`]
      });
      console.log(appToken)
      const graphToken = await instance.acquireTokenSilent({
        scopes: ["User.Read"]
      });
      console.log(graphToken)
      this.setState({ userFullName: accounts[0].name ? accounts[0].name : "N/A" })
      let res = await axios.get(`https://graph.microsoft.com/v1.0/users/${accounts[0].username}/photo/$value`, this.axiosConfigImage(graphToken.accessToken));

      let buff = Buffer.from(res.data, 'binary').toString('base64');
      let image = buff ? ("data:image/jpeg;base64," + buff) : "";
      this.setState({ userImage: image })

      let res2 = await axios.get("https://34.110.195.195/home", this.axiosConfig(appToken.accessToken))
      console.log(res2)
      this.setState({sqlData:res2.data});
    }
  }

  render() {
    const { classes, instance } = this.props;
    let {sqlData, snackOpen, dialogOpen, userFullName, userImage, tab } = this.state;
    return (
      <div className={classes.root}>
        <AppBar elevation={0} position="fixed" style={{ backgroundColor: grey[900] }}  >
          <Toolbar>
            <div className={classes.logoContainer}>
              <img src={logo} alt="logo" className={classes.logoImage} />
            </div>
            <Typography noWrap variant={"h5"} style={{ fontWeight: 800 }}>
              App
            </Typography>
            <div style={{ flexGrow: 1 }}></div>
            <Hidden xsDown>
              <Typography variant="body1" noWrap >
                Hello, {userFullName}
              </Typography>
            </Hidden>
            <Button color={"secondary"} onClick={() => { instance.logout() }} className={classes.noButtonWrap}>Log Out</Button>
            <IconButton color="primary" size="small">
              <Avatar src={userImage} ></Avatar>
            </IconButton>
          </Toolbar>
        </AppBar>

        <main className={classes.content}>
          <Toolbar />
          {true && <AppBar position={"static"} >
            <Tabs value={tab} onChange={(e: any, tab: any) => { this.setState({ tab: tab }) }} >
              <Tab label="Form Inputs" />
              <Tab label="Dialog" />
              <Tab label="Cloud SQL" />
              <Tab label="Table" />
              <Tab label="Table Pro" />
            </Tabs>
          </AppBar>
          }
          <div className={classes.mainContent}>
            {true && tab === 0 &&
              <Grid container spacing={2} justifyContent='center' alignItems="center">
                <Grid item>
                  <Card style={{ minWidth: 275 }}>
                    <CardContent>
                      <Grid container spacing={2}>
                        <Grid item xs={12}>
                          <TextField label="Outlined" variant="outlined" fullWidth />
                        </Grid >
                        <Grid item xs={12}>
                          <TextField label="Filled" variant="filled" fullWidth />
                        </Grid>
                        <Grid item xs={12}>
                          <TextField label="Standard" variant="standard" fullWidth />
                        </Grid>
                        <Grid item xs={12}>
                          <FormControl fullWidth>
                            <InputLabel>Age</InputLabel>
                            <Select
                              label="Age"
                            >
                              <MenuItem value={10}>Ten</MenuItem>
                              <MenuItem value={20}>Twenty</MenuItem>
                              <MenuItem value={30}>Thirty</MenuItem>
                            </Select>
                          </FormControl>
                        </Grid>
                        <Grid item xs={12}>
                          <FormControl>
                            <FormLabel >Options</FormLabel>
                            <RadioGroup
                              color={"primary"}
                              defaultValue="female"
                            >
                              <FormControlLabel value="Option 1" control={<Radio color={"primary"} />} label="Option 1" />
                              <FormControlLabel value="Option 2" control={<Radio color={"primary"} />} label="Option 2" />
                              <FormControlLabel value="Option 3" control={<Radio color={"primary"} />} label="Option 3" />
                            </RadioGroup>
                          </FormControl>
                        </Grid>
                        <Grid item xs={12}>
                          <Slider color={"primary"} defaultValue={50} aria-label="Default" valueLabelDisplay="auto" />
                        </Grid>
                        <Grid item xs={12}>
                          <FormGroup>
                            <FormControlLabel control={<Checkbox color={"primary"} defaultChecked />} label="Label" />
                            <FormControlLabel disabled control={<Checkbox color={"primary"} />} label="Disabled" />
                          </FormGroup>
                        </Grid>
                      </Grid>
                    </CardContent>
                    <CardActions>
                      <Button variant={"outlined"} >Learn More</Button>
                      <div style={{ flexGrow: 1 }}></div>
                      <Button color={"primary"} variant={"contained"} >Learn More</Button>
                    </CardActions>
                  </Card>
                </Grid>
              </Grid>
            }
            {tab === 1 &&
              <Grid container spacing={2} justifyContent='center' alignItems="center">
                <Grid item>
                  <Button variant="outlined" onClick={() => { this.setState({ dialogOpen: true }) }}>
                    Open form dialog
                  </Button>
                </Grid>
              </Grid>
            }
            {tab === 2 &&
              <React.Fragment>
                <Grid container spacing={2} justifyContent='center' alignItems="center">
                  <Grid item >
                    <Button variant="outlined" onClick={async () => {
                      const appToken = await instance.acquireTokenSilent({
                        scopes: [`api://85a05ab5-2804-4f54-819f-b19ef151b973/access`]
                      });
                      await axios.get("https://34.110.195.195/home/addrow", this.axiosConfig(appToken.accessToken))
                      let res2 = await axios.get("https://34.110.195.195/home", this.axiosConfig(appToken.accessToken))
                      this.setState({sqlData:res2.data});
                    }}>
                      Add Row
                    </Button>
                  </Grid>
                </Grid>
                <Grid container spacing={2} justifyContent='center' alignItems="center">
                  <Grid item >
                    <List>
                      {sqlData.map((i)=>(<ListItem>
                        <ListItemText
                          primary={i.Name}
                          secondary={i.User_ID}
                        />
                      </ListItem>))}
                    </List>
                  </Grid>
                </Grid>
              </React.Fragment>
            }
            {tab === 3 &&
                  <DataGridContainer/>
            }
            {tab === 4 &&
                  <DataGridContainerPro/>
            }
            <Dialog open={dialogOpen} onClose={() => { this.setState({ dialogOpen: false }) }}>
              <DialogTitle>Subscribe</DialogTitle>
              <DialogContent>
                <DialogContentText>
                  To subscribe to this website, please enter your email address here. We
                  will send updates occasionally.
                </DialogContentText>
                <TextField
                  autoFocus
                  margin="dense"
                  id="name"
                  label="Email Address"
                  type="email"
                  fullWidth
                  variant="standard"
                />
              </DialogContent>
              <DialogActions>
                <Button onClick={() => { this.setState({ dialogOpen: false }) }}>Cancel</Button>
                <Button onClick={() => { this.setState({ dialogOpen: false }) }}>Subscribe</Button>
              </DialogActions>
            </Dialog>
          </div>
        </main>
      </div>
    );
  }
}
export default withRoot(withStyles(styles)(App));
